$(function() {
    $('footer').footerReveal({
        shadowOpacity: .2,
        zIndex: -101
    });
});
AOS.init({
    offset: 100,
    duration: 600,
    easing: 'ease-in-out-cubic',
    delay: 50,
	once: true
});

var btn = $('.back-to-top');

$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: 0
    }, '300');
});
$('#right-button').click(function(event) {
  event.preventDefault();
  $('.results__plan').animate({
    scrollLeft: "+=800px"
  }, "slow");
});

 $('#left-button').click(function(event) {
  event.preventDefault();
  $('.results__plan').animate({
    scrollLeft: "-=800px"
  }, "slow");
});